enum Flavor {
  PRODUCTION,
  DEV,
  STAG,
  YASHRAJ,
}

class F {
  static Flavor? appFlavor;

  static String get title {
    switch (appFlavor) {
      case Flavor.PRODUCTION:
        return 'Go Kozo';
      case Flavor.DEV:
        return 'Go Kozo DEV';
      case Flavor.STAG:
        return 'Go Kozo STAG';
      case Flavor.YASHRAJ:
        return 'YRF Kozo';
      default:
        return 'title';
    }
  }

}
